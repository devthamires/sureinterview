export type ButtonType = 'button' | 'submit';

export type ButtonClass = 'success' | 'primary' | 'warning';
