import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input() title!: string;
  @Input() imgUrl!: string;
  @Input() isActive: boolean = true;
  @Input() date!: Date;

  @Output() onToogle = new EventEmitter<void>();
  @Output() onLike = new EventEmitter<void>();
}
