import { Component, Input } from '@angular/core';
import { ButtonClass, ButtonType } from '../../models/types/buttons';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() disabled:boolean = false;
  @Input() className: ButtonClass = 'primary';
  @Input() type: ButtonType = 'button';
}
