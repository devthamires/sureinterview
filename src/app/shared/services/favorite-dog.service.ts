import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Dog } from '../models/types/dog';

@Injectable({
  providedIn: 'root',
})
export class FavoriteDogService {
  dogs$ = new BehaviorSubject<Dog[]>([]);

  addDog(dog: Dog) {
    this.dogs$.next([dog, ...this.dogs$.value]);
  }
}
