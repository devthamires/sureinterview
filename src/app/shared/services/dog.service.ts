import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Dog } from '../models/types/dog';

@Injectable({
  providedIn: 'root',
})
export class DogService {
  private readonly url = 'https://dog.ceo/api/breeds/image/random';

  constructor(private http: HttpClient) {}

  getDog(): Observable<Dog> {
    return this.http.get<Dog>(this.url);
  }
}
