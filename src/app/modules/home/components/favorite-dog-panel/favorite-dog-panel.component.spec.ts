import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteDogPanelComponent } from './favorite-dog-panel.component';

describe('FavoriteDogPanelComponent', () => {
  let component: FavoriteDogPanelComponent;
  let fixture: ComponentFixture<FavoriteDogPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FavoriteDogPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteDogPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
