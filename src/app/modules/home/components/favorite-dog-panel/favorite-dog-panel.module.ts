import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoriteDogPanelComponent } from './favorite-dog-panel.component';

@NgModule({
  declarations: [FavoriteDogPanelComponent],
  imports: [CommonModule],
  exports:[FavoriteDogPanelComponent]
})
export class FavoriteDogPanelModule {}
