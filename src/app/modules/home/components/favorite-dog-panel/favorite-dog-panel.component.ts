import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Dog } from 'src/app/shared/models/types/dog';
import { FavoriteDogService } from 'src/app/shared/services/favorite-dog.service';

@Component({
  selector: 'app-favorite-dog-panel',
  templateUrl: './favorite-dog-panel.component.html',
  styleUrls: ['./favorite-dog-panel.component.scss'],
})
export class FavoriteDogPanelComponent implements OnInit {
  myFavoriteDogs$!: Observable<Dog[]>;

  constructor(private favoriteDogService: FavoriteDogService) {}

  ngOnInit(): void {
    this.myFavoriteDogs$ = this.favoriteDogService.dogs$;
  }
}
