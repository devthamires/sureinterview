import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, interval, Observable } from 'rxjs';
import {
  filter,
  startWith,
  switchMap,
  take,
  takeWhile,
  tap,
} from 'rxjs/operators';
import { Dog } from 'src/app/shared/models/types/dog';
import { DogService } from 'src/app/shared/services/dog.service';
import { FavoriteDogService } from 'src/app/shared/services/favorite-dog.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  dog$!: Observable<Dog>;
  isStreamOn: boolean = true;
  today!: Date;

  constructor(
    private dogService: DogService,
    private favoriteDogService: FavoriteDogService
  ) {}

  ngOnInit(): void {
    this.dog$ = interval(3000).pipe(
      startWith(0),
      filter(() => this.isStreamOn),
      tap(() => (this.today = new Date())),
      switchMap(() => this.dogService.getDog())
    );
  }

  toggle() {
    this.isStreamOn = !this.isStreamOn;
  }

  favorite(dog: Dog) {
    this.favoriteDogService.addDog(dog);
  }
}
