import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { HeaderModule } from './components/header/header.module';
import { FavoriteDogPanelModule } from './components/favorite-dog-panel/favorite-dog-panel.module';
import { CardModule } from 'src/app/shared/components/card/card.module';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HeaderModule,
    FavoriteDogPanelModule,
    CardModule
  ]
})
export class HomeModule { }
